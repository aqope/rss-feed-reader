<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Regular / Route */
Route::get('/', 'IndexController@index');

/** Ajax Register Route */
Route::post('/register', ['as' => 'register', 'uses' => 'RegisterController@register']);

/** Login Route */
Route::post('/login', ['as' => 'login', 'uses' => 'LoginController@login']);

/** Login Route */
Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@index']);

/** Logout Route */
Route::get('/logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
