@extends('layouts.app')

@section('content')
    <main-screen
            :top-words="{{ json_encode($topWords) }}"
            :entries="{{ json_encode($entries) }}"
            :logout-url="{{ json_encode($logoutUrl) }}"
    ></main-screen>
@endsection