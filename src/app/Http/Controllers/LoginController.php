<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the welcome view (login)
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }


    /**
     * Failed Login Ajax Response
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendFailedLoginResponse($request) {
        throw ValidationException::withMessages([
            'message' => 'Incorrect Password or User!'
        ]);
    }

    /**
     * Login Ajax Response
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendLoginResponse($request) {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: response()->json(['status' => 'OK', 'action' => 'redirect', 'url' => $this->redirectPath()]);
    }
}
