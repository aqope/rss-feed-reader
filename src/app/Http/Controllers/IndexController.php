<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\API\Handler;
use App\API\Counter;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the main app view
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $counter = new Counter();
        $handler = new Handler(config('api.url'));
        $counter->handle($handler->getData());
        $apiData = $handler->getData();

        return view('main',
            ['topWords' => $counter->getTopWords(), 'entries' => $apiData['entries'], 'logoutUrl' => url('logout')]
        );
    }
}
