<?php

namespace App\API;

use GuzzleHttp\Client;

/**
 * Class Handler
 * @package App\API
 */
class Handler
{
    /**
     * @var
     */
    protected $url;

    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $data;

    const REQ_TYPE_GET = 'GET';

    /**
     * Handler constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * API Data fetching through client
     *
     * @param $action
     * @param string $type
     * @return mixed
     */
    protected function fetch($action, $type = self::REQ_TYPE_GET) {
        $resource = $this->client->request($type, $action);

        return $resource->getBody()
            ->getContents();
    }

    /**
     * Simple Parser for the incoming response
     *
     * @param $response
     * @return array
     */
    protected function parse($response): array {
        $data = new \SimpleXMLElement($response);

        $parsed = [
            'id' => (string)$data->id,
            'title' => (string)$data->title,
            'rights' => (string)$data->rights,
            'author' => [
                'name' => (string)$data->author->name,
                'email' => (string)$data->author->email,
                'uri' => (string)$data->author->uri,
            ],
            'subtitle' => (string)$data->subtitle,
            'logo' => (string)$data->logo,
            'updated' => (string)$data->updated,
            'entries' => [],
        ];

        foreach ($data->entry as $item) {
            $entry = [
                'id' => (string)$item->id,
                'updated' => (string)$item->updated,
                'author' => (string)$item->author->name,
                'author_uri' => (string)$item->author->uri,
                'title' => (string)$item->title,
                'summary' => (string)strip_tags($item->summary)
            ];

            $parsed['entries'][] = $entry;
        }

        return $parsed;
    }

    /**
     * Prepare Client
     */
    protected function prepare(): void {
        $this->client = new Client([]);
    }

    /**
     * Get API Data
     *
     * @return array
     */
    public function getData($refetch = false): array {
        if (empty($this->data) || $refetch === true) {
            if (empty($this->client)) {
                $this->prepare();
            }

            $this->data = $this->parse(
                $this->fetch($this->url)
            );
        }

        return $this->data;
    }
}