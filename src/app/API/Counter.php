<?php

namespace App\API;

use Illuminate\Support\Arr;

/**
 * Class Counter
 * @package App\API
 */
class Counter
{
    /**
     * Main Counter data
     *
     * @var array
     */
    public $wordCounter = [];

    /**
     * Data Keys from which to exclude counting
     *
     * @var array
     */
    protected $excludeKeys = [
        'name',
        'email',
        'id',
        'logo',
        'updated',
        'uri',
        'author_uri'
    ];

    /**
     * Handles word counting for the given $data array
     *
     * @param $data
     */
    public function handle($data) {
        foreach ($data as $key => $element) {
            if (in_array($key, $this->excludeKeys)) {
                continue;
            } else {
                if (is_array($element)) {
                    $this->handle($element);
                } elseif(is_string($element)) {
                    $this->count($element);
                }
            }
        }
        arsort($this->wordCounter);
    }

    /**
     * Filters words and counts them appearance in the given $content
     *
     * @param $content
     */
    protected function count($content) {
        $words = [];

        preg_match_all("/('\w+)|(\w+'\w+)|(\w+')|(\w+)/", $content, $words);

        foreach ($words[0] as $element) {
            $element = strtolower($element);
            if (in_array($element, config('blacklist.to_exclude'))) {
                continue;
            }

            if (is_numeric($element)) {
                continue;
            }

            if (empty($element)) {
                continue;
            }

            if (array_key_exists($element, $this->wordCounter)) {
                $this->wordCounter[$element] = $this->wordCounter[$element] + 1;
            } else {
                $this->wordCounter[$element] = 1;
            }
        }
    }

    /**
     * Returns top 10 most used words in the previous counted
     * data array
     *
     * @return array
     */
    public function getTopWords() {
        $words = [];
        foreach (array_splice($this->wordCounter, 0, 10) as $key => $item) {
            $words[] = [
                'word' => $key,
                'count' => $item,
            ];
        }

        return $words;
    }
}