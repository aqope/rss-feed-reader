# Feedy (mintos task)

### How to set up project
1. Install composer and node dependencies:
    * cd ~/src/
    * docker run --rm -v $(pwd):/app composer install
    * npm install
2. Copy environment files for the Docker and Laravel
    * cp .env.example .env
    * cp src/.env.example src/.env
3. Build containers and put the up
    * docker-compose up
4. Compile Styles and Vue Templates
    * npm run dev
5. Execute migrations
    * docker exec -it feedy_app bash
    * cd /var/www/
    * php artisan key:generate
    * php artisan migrate
6. Open http://localhost:8080/ and observe functionality

---

### Contributors
Artur Paklin

